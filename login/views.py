from django.urls import reverse
from django.shortcuts import redirect, render
from django.http import HttpResponseRedirect
from django.contrib.auth import login as loginMethod, authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import logout


def home(request):
    return render(request, 'login/home.html')


def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            loginMethod(request, user)
            return HttpResponseRedirect(reverse('vacations:my_vacations'))
    else:
        form = AuthenticationForm(request.POST)
    return render(request, 'login/login.html', {'form' : form})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            loginMethod(request, user)
            return HttpResponseRedirect(reverse('vacations:my_vacations'))
    else:
        form = UserCreationForm(request.POST)
    return render(request, 'login/signup.html', { 'form' : form })


def logout_view(request):
    logout(request)
    return render(request, 'login/home.html')