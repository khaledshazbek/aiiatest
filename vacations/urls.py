from django.urls import path
from vacations import views


app_name = 'vacations'
urlpatterns = [
    path('my_vacations/', views.myVacation, name='my_vacations'),
    path('vacations_forms/', views.vacationForm, name='vacations_forms'),
]   