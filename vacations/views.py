from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
import json

from vacations.models import VacationModel
from . import forms

# Create your views here.
@login_required(login_url="/login/")
def myVacation(request):
    data = VacationModel.objects.filter(creator= request.user).order_by('startDate').values()
    result = json.dumps({"data": list(data)}, default=str)
    resp = {"data":result}
    return render(request, 'vacations/my_vacations.html', resp)


@login_required(login_url="/login/")
def vacationForm(request):
    if request.method == 'POST':
        form = forms.VacationForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.creator = request.user
            instance.duration = instance.vacationDuration()
            instance.save()
            return redirect('vacations:my_vacations') 
    else:
        form = forms.VacationForm() 
    return render(request, 'vacations/vacations_form.html', {'form':form})