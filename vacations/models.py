from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class VacationModel(models.Model):
    description = models.CharField(max_length=200, null=False)
    startDate = models.DateField(null=False)
    endDate = models.DateField(null=False)
    duration = models.PositiveIntegerField(null=False)
    creator = models.ForeignKey(User, default=None, on_delete=models.CASCADE)

    def vacationDuration(self):
        return (self.endDate - self.startDate).days 

    def __str__(self):
        return self.description