from django import forms
from . import models
from bootstrap_datepicker_plus.widgets import DatePickerInput

class VacationForm(forms.ModelForm):
    class Meta:
        model = models.VacationModel
        fields = ['description', 'startDate', 'endDate']
        widgets = {
            'startDate': DatePickerInput(),
            'endDate': DatePickerInput(), 
        }